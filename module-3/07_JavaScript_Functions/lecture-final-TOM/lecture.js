/**
 * All named functions will have the function keyword and
 * a name followed by parentheses.
 * 
 * @returns {number} 1
 */
function returnOne() {
  return 1;
}

/**
 * Functions can also take parameters. These are just variables that are filled
 * in by whomever is calling the function.
 *
 * Also, we don't *have* to return anything from the actual function.
 *
 * @param {any} value the value to print to the console
 */
function printToConsole(value) {
  console.log(value);
}

/**
 * Write a function called multiplyTogether that multiplies two numbers together. But 
 * what happens if we don't pass a value in? What happens if the value is not a number?
 *
 * @param {number} firstParameter the first parameter to multiply
 * @param {number} secondParameter the second parameter to multiply
 */
function multiplyTogether(firstParameter, secondParameter) {
  return secondParameter * firstParameter;
}

/**
 * This version makes sure that no parameters are ever missing. If
 * someone calls this function without parameters, we default the
 * values to 0. However, it is impossible in JavaScript to prevent
 * someone from calling this function with data that is not a number.
 * Call this function multiplyNoUndefined
 *
 * @param {number} [firstParameter=0] the first parameter to multiply
 * @param {number} [secondParameter=0] the second parameter to multiply
 */
 function multiplyNoUndefined(firstParameter = 0, secondParameter = 0) {
  return secondParameter * firstParameter;
}


 
/**
 * Functions can return earlier before the end of the function. This could be useful
 * in circumstances where you may not need to perform additional instructions or have to
 * handle a particular situation.
 * In this example, if the firstParameter is equal to 0, we return secondParameter times 2.
 * Observe what's printed to the console in both situations.
 * 
 * @param {number} firstParameter the first parameter
 * @param {number} secondParameter the second parameter
 */
function returnBeforeEnd(firstParameter, secondParameter) {
  console.log("This will always fire.");

  if (firstParameter == 0) {
    console.log("Returning secondParameter times two.");
    return secondParameter * 2;
  }

  //this only runs if firstParameter is NOT 0
  console.log("Returning firstParameter + secondParameter.");
  return firstParameter + secondParameter;
}

/**
 * Scope is defined as where a variable is available to be used.
 *
 * If a variable is declare inside of a block, it will only exist in
 * that block and any block underneath it. Once the block that the
 * variable was defined in ends, the variable disappears.
 */
function scopeTest() {
  // This variable will always be in scope in this function
  let inScopeInScopeTest = true;

  {
    // this variable lives inside this block and doesn't
    // exist outside of the block
    const scopedToBlock = inScopeInScopeTest;
  }

  // scopedToBlock doesn't exist here so an error will be thrown
  if (inScopeInScopeTest && scopedToBlock) {
    console.log("This won't print!");
  }
}

function createSentenceFromUser(name, age, listOfQuirks = [], separator = ', ') {
  const description = `${name} is currently ${age} years old. Their quirks are: `;
  return description + listOfQuirks.join(separator);
}

/**
 * Takes an array and, using the power of anonymous functions, generates
 * their sum.
 *
 * @param {number[]} numbersToSum numbers to add up
 * @returns {number} sum of all the numbers
 */
function sumAllNumbers(numbersToSum = []) {
  console.log(numbersToSum);
  let counter = 1;
  let finalValue = numbersToSum.reduce((r, number) => {
    console.log(`${counter++} -- Reducer: ${r} Element: ${number}`);
    return r + number;
  }, 0);
  console.log("Final Value: " + finalValue);
  return finalValue;
}

/**
 * Takes an array and returns a new array of only numbers that are
 * multiples of 3
 *
 * @param {number[]} numbersToFilter numbers to filter through
 * @returns {number[]} a new array with only those numbers that are
 *   multiples of 3
 */
function allDivisibleByThree(numbersToFilter = []) {
  return numbersToFilter.filter((number) => {
    if (number % 3 === 0) {
      return "Hey Man, Nice Filter!";
    }
    return 0;
  });
}


function example() {

  // 4 Array Fxns
  // 1. forEach() -> no return value
  // 2. map() -> returns array (always of the same size)
  // 3. filter() -> returns array (not always of the same size)
  // 4. reduce() -> returns a single value (NOT AN ARRAY)

  let arr = [10, 11, 12, 13, 14, 15];
  let resultArr;


  // forEach(fxn);
  resultArr = arr.forEach((element) => {
    console.log(element);
  });
  console.log("forEach returned: " + resultArr);


  // map(fxn);  -> returns array of same size, but with transformed values
  resultArr = arr.map((element) => {
    return element * 2;
  });
  console.log("Arr: " + arr);
  console.log("map returned: " + resultArr.join(", "));


  // filter(fxn);  -> returns array of the elements that returned truthy when passed through fxn
  resultArr = arr.filter((element) => { 
    return typeof element === "number";    
  });
  console.log("Arr: " + arr);
  console.log("filter returned: " + resultArr.join(", "));


  // reduce(fxn, initialReducer);   -> returns single accumulated value
  console.log(arr);
  let counter = 1;

  //                        accumulator  current
  let resultValue = arr.reduce((reducer, element) => {
    console.log(`${counter++} -- Reducer: ${reducer} Element: ${element}`);
    return element + reducer;
  }, 0);

  console.log("Arr: " + arr);
  console.log("reduce returned: " + resultValue);

  return "Ignore this!";
}
