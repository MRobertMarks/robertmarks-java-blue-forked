const site = 'https://techelevator-pgh-teams.azurewebsites.net/api/techelevator/shoppinglist'
document.addEventListener('DOMContentLoaded', () => {
    const lb = document.querySelector('.loadingButton');
    const groceryList = document.getElementById('grocery-list')
    let groceriesLoaded = false;
    lb.addEventListener('click', event => {
        if (groceriesLoaded == false) {
        fetch (site)
        .then((response) => {
             return response.json();
        })
        .then( (items) => {
                items.forEach( (item) => {
                    const li = document.createElement('li');
                    li.innerText = item.name;
                    const circleCheck = document.createElement('i');
                    circleCheck.setAttribute('class', 'far fa-check-circle');
                    if (item.completed == true) {
                        circleCheck.classList.add('completed');
                    }
                    li.appendChild(circleCheck);
                    groceryList.appendChild(li);
                })
            })
            groceriesLoaded = true;
        }
    })
})