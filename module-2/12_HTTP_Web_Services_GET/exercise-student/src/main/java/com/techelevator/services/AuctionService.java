package com.techelevator.services;

import org.springframework.web.client.RestTemplate;

import com.techelevator.model.Auction;

public class AuctionService {

    public final String API_URL;
    public final String API_KEY;
    public RestTemplate restTemplate = new RestTemplate();
    private final ConsoleService console = new ConsoleService();

    public AuctionService(String apiUrl, String apiKey) {
        API_URL = apiUrl;
        API_KEY = apiKey;
    }

    public Auction[] listAllAuctions() {
        return restTemplate.getForObject(API_URL + "auctions?apikey=" + API_KEY, Auction[].class);
    }

    public Auction listDetailsForAuction(int id) {
        return restTemplate.getForObject(API_URL + "auctions/" + id + "/details?apikey=" + API_KEY, Auction.class);
    }

    public Auction[] findAuctionsSearchTitle(String title) {
        return restTemplate.getForObject(API_URL + "auctions?apikey=" + API_KEY + "&title=" + title, Auction[].class);
    }

    public Auction[] findAuctionsSearchPrice(double price) {
        return restTemplate.getForObject(API_URL + "auctions?apikey=" + API_KEY + "&price=" + price, Auction[].class);
    }

}
