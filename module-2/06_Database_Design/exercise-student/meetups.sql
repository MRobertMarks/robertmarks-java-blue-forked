BEGIN TRANSACTION;

DROP TABLE IF EXISTS group_member;
DROP TABLE IF EXISTS event_member;
DROP TABLE IF EXISTS event_group;

DROP TABLE IF EXISTS interest_group;
DROP TABLE IF EXISTS meet_member;
DROP TABLE IF EXISTS meet_event;


CREATE TABLE meet_member (
member_number serial,--PK
last_name varchar (60) not null,
first_name varchar (60) not null,
email_address varchar (60)not null,
Phone_number varchar (15) null,-- (some members may not provide one)
date_of_birth date not null,
reminder_email_flag boolean,

CONSTRAINT pk_meet_member PRIMARY KEY (member_number)

);
 
CREATE TABLE interest_group (
group_number serial, --PK
--member_number bigint, -- Has zero-to-many Members
group_name varchar (60) UNIQUE not null,-- (no two groups can have the same name)


CONSTRAINT pk_interest_group PRIMARY KEY (group_number)
--CONSTRAINT fk_interest_group_meet_member FOREIGN KEY (member_number) REFERENCES meet_member (member_number)

);

CREATE TABLE meet_event (
event_number serial, --PK
-- member_number bigint, -- Has zero-to-many Members
-- group_number bigint, -- running this event
event_name varchar (60) not null,
event_description varchar (500) not null,
event_start_Date_time timestamp not null,
event_duration  int default (30) not null,-- (minimum of 30 minutes)
-- event_owner_group_name varchar (60), -- running this event

CONSTRAINT pk_meet_event PRIMARY KEY (event_number)
--CONSTRAINT fk_meet_event_meet_member FOREIGN KEY (member_number) REFERENCES meet_member (member_number),
--CONSTRAINT fk_meet_event_interest_group FOREIGN KEY (group_number) REFERENCES interest_group (group_number)
--constraint event_duration >= 30

);

CREATE TABLE group_member (
group_number bigint,
member_number bigint,
CONSTRAINT fk_group_member_group FOREIGN KEY (group_number) REFERENCES interest_group (group_number),
CONSTRAINT fk_group_member_member FOREIGN KEY (member_number) REFERENCES meet_member (member_number)
);

CREATE TABLE event_member (
event_number bigint,
member_number bigint,
CONSTRAINT fk_event_member_event FOREIGN KEY (event_number) REFERENCES meet_event (event_number),
CONSTRAINT fk_event_member_member FOREIGN KEY (member_number) REFERENCES meet_member (member_number)
);

CREATE TABLE event_group (
event_number bigint,
group_number bigint,
CONSTRAINT fk_event_member_event FOREIGN KEY (event_number) REFERENCES meet_event (event_number),
CONSTRAINT fk_meet_event_interest_group FOREIGN KEY (group_number) REFERENCES interest_group (group_number)
);

-- All tables must have a primary key.
-- Populate the tables with data for at least four events, three groups, and eight members.

INSERT INTO meet_member (last_name, first_name, email_address, Phone_number, date_of_birth, reminder_email_flag)
VALUES ('Marks', 'M. Robert', 'MRobertMarks@gmail.com', '412.313.4304', '1989-03-19', true);
INSERT INTO meet_member (last_name, first_name, email_address, Phone_number, date_of_birth, reminder_email_flag)
VALUES ('Doe', 'John', 'JohnDoe@fakemail.com', null, '1901-10-22', false);
INSERT INTO meet_member (last_name, first_name, email_address, Phone_number, date_of_birth, reminder_email_flag)
VALUES ('Doe', 'Jane', 'IDontKnowJohn@jane.com', '555-867-5309', '1991-10-22', true);
INSERT INTO meet_member (last_name, first_name, email_address, Phone_number, date_of_birth, reminder_email_flag)
VALUES ('Olzewski', 'Raymond', 'RayCO@gmail.com', '(412)921-1029', '1922-07-20', true);
INSERT INTO meet_member (last_name, first_name, email_address, Phone_number, date_of_birth, reminder_email_flag)
VALUES ('Doe', 'James', 'JaneAndJohnAreLying@BelieveMe.com', '(412)-I-Forgot', '2002-01-01', false);
INSERT INTO meet_member (last_name, first_name, email_address, Phone_number, date_of_birth, reminder_email_flag)
VALUES ('Trump', 'Donald', 'RubMyBelly@potus.com', 'call Fox News', '1984-09-11', true);
INSERT INTO meet_member (last_name, first_name, email_address, Phone_number, date_of_birth, reminder_email_flag)
VALUES ('Who', 'Is On First', 'Abbott@Costello.com', '123456789', '1953-01-01', false);
INSERT INTO meet_member (last_name, first_name, email_address, Phone_number, date_of_birth, reminder_email_flag)
VALUES ('What', 'Is On Second', 'Costello@Abbott.com', '987654321', '1953-01-01', true);

INSERT INTO interest_group (group_name)
VALUES ('Data Base Criers');
INSERT INTO interest_group (group_name)
VALUES ('Java Juveniles');
INSERT INTO interest_group (group_name)
VALUES ('Java Blue');

INSERT INTO meet_event (event_name, event_description, event_start_Date_time, event_duration)
VALUES ('Sesame Street Horror House', 'Best time with a chainsaw you have ever had!', '2021-05-09 00:00:00', '45');
INSERT INTO meet_event (event_name, event_description, event_start_Date_time, event_duration)
VALUES ('Robert''s Birthday!', 'nothing interesting really', '2022-03-19 00:00:00', '1440');
INSERT INTO meet_event (event_name, event_description, event_start_Date_time, event_duration)
VALUES ('West End Overlook Study Picnic', 'It is actually a group to collectively grieve and complain about our homework', '2021-06-15 15:00:00', '240');
INSERT INTO meet_event (event_name, event_description, event_start_Date_time, event_duration)
VALUES ('Tech Elevator', 'Say goodbye to your family and friends for 3 months', '2021-05-10 09:00:00', '120');

-- Make sure each event has at least one member assigned to it, and each group has at least two members.

--UPDATE group_member set member_number = (SELECT member_number FROM meet_member WHERE last_name = 'Marks')
--WHERE group_name = 'Java_Blue';

INSERT INTO group_member (group_number, member_number)
VALUES ((SELECT group_number FROM interest_group WHERE group_name = 'Java Juveniles'),(SELECT member_number FROM meet_member WHERE last_name = 'Trump'));
INSERT INTO group_member (group_number, member_number)
VALUES ((SELECT group_number FROM interest_group WHERE group_name = 'Java Juveniles'),(SELECT member_number FROM meet_member WHERE first_name = 'James'));

INSERT INTO group_member (group_number, member_number)
VALUES ((SELECT group_number FROM interest_group WHERE group_name = 'Java Blue'),(SELECT member_number FROM meet_member WHERE last_name = 'Olszewski'));
INSERT INTO group_member (group_number, member_number)
VALUES ((SELECT group_number FROM interest_group WHERE group_name = 'Java Blue'),(SELECT member_number FROM meet_member WHERE last_name = 'Marks'));

INSERT INTO group_member (group_number, member_number)
VALUES ((SELECT group_number FROM interest_group WHERE group_name = 'Data Base Criers'),(SELECT member_number FROM meet_member WHERE last_name = 'Who'));
INSERT INTO group_member (group_number, member_number)
VALUES ((SELECT group_number FROM interest_group WHERE group_name = 'Data Base Criers'),(SELECT member_number FROM meet_member WHERE last_name = 'What'));


INSERT INTO event_member (event_number, member_number)
VALUES ((SELECT event_number FROM meet_event WHERE event_name = 'Sesame Street Horror House'),(SELECT member_number FROM meet_member WHERE last_name = 'Trump'));
INSERT INTO event_member (event_number, member_number)
VALUES ((SELECT event_number FROM meet_event WHERE event_name = 'Robert''s Birthday!'),(SELECT member_number FROM meet_member WHERE last_name = 'Marks'));
INSERT INTO event_member (event_number, member_number)
VALUES ((SELECT event_number FROM meet_event WHERE event_name = 'West End Overlook Study Picnic'),(SELECT member_number FROM meet_member WHERE last_name = 'Who'));
INSERT INTO event_member (event_number, member_number)
VALUES ((SELECT event_number FROM meet_event WHERE event_name = 'Tech Elevator'),(SELECT member_number FROM meet_member WHERE last_name = 'What'));

COMMIT;