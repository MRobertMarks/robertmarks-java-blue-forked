package com.techelevator.dao;

// Domain Object

// POJO:  Plain Ol' Java Object
public class Movie {
    private String title = "";
    private String tagline = "";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }
}
