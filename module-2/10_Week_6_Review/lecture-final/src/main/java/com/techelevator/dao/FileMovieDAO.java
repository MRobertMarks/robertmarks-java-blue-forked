package com.techelevator.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileMovieDAO implements MovieDAO {

    private Scanner fileStreamer;
    private List<Movie> allMovies = new ArrayList<Movie>();

    public FileMovieDAO() {
        File inputFile = new File("movies.csv");
        try {
            fileStreamer = new Scanner(inputFile);
            while (fileStreamer.hasNext()) {
                String thisLine = fileStreamer.nextLine();
                allMovies.add(mapLineToMovie(thisLine));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Could not open file!");
        }
    }

    @Override
    public List<Movie> getAllMovies() {
        return new ArrayList<>(allMovies);
    }

    @Override
    public List<Movie> getMatchingMovies(String searchTerm) {
        List<Movie> matchingMovies = new ArrayList<>();

        for (Movie m : allMovies) {
            String title = m.getTitle().toLowerCase();
            String tagline = m.getTagline().toLowerCase();

            if (title.contains(searchTerm.toLowerCase()) || tagline.contains(searchTerm.toLowerCase())) {
                matchingMovies.add(m);
            }
        }

        return matchingMovies;
    }

    private Movie mapLineToMovie(String line) {
        Movie m = new Movie();

        if (line != null) {
            String[] pieces = line.split("\\|");

            if (pieces.length > 0) {
                m.setTitle(pieces[0].trim());
            }

            if (pieces.length > 1) {
                m.setTagline(pieces[1].trim());
            }
        }

        return m;
    }
}
