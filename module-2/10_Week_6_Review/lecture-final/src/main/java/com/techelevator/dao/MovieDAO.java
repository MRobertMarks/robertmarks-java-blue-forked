package com.techelevator.dao;

import java.util.List;

public interface MovieDAO {
    List<Movie> getAllMovies();
    List<Movie> getMatchingMovies(String searchTerm);
}
