package com.techelevator.dao;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.util.ArrayList;
import java.util.List;

public class JDBCMovieDAO implements MovieDAO {

    private JdbcTemplate movieConnection;

    public JDBCMovieDAO() {
        BasicDataSource movieDatabase = new BasicDataSource();
        movieDatabase.setUrl("jdbc:postgresql://localhost:5432/MovieDB");
        movieDatabase.setUsername("postgres");
        movieDatabase.setPassword("postgres1");

        movieConnection = new JdbcTemplate(movieDatabase);
    }

    @Override
    public List<Movie> getAllMovies() {
        String sql = "SELECT title, tagline FROM movie;";

        SqlRowSet results = movieConnection.queryForRowSet(sql);

        return mapsRowSetToListOfMovies(results);
    }

    @Override
    public List<Movie> getMatchingMovies(String searchTerm) {
        String sql = "SELECT title, tagline " +
                     "FROM movie " +
                     "WHERE title ILIKE ? OR tagline ILIKE ? ;";

        SqlRowSet results = movieConnection.queryForRowSet(sql, "%" + searchTerm + "%", "%" + searchTerm + "%");

        return mapsRowSetToListOfMovies(results);
    }

    private List<Movie> mapsRowSetToListOfMovies(SqlRowSet results) {
        List<Movie> output = new ArrayList<>();
        while(results.next()) {
            output.add(mapRowToMovie(results));
        }
        return output;
    }

    private Movie mapRowToMovie(SqlRowSet row) {
        String title = row.getString("title");
        String tagline = row.getString("tagline");

        Movie m = new Movie();
        m.setTitle(title);
        m.setTagline(tagline);
        return m;
    }
}
