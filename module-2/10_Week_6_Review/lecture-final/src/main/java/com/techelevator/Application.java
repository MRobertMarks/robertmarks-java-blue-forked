package com.techelevator;

import com.techelevator.dao.FileMovieDAO;
import com.techelevator.dao.JDBCMovieDAO;
import com.techelevator.dao.Movie;
import com.techelevator.dao.MovieDAO;

import java.util.List;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        System.out.println("Let's go out to the movies!");
        System.out.print("Enter a movie for which to search >>> ");
        String userSearchTerm = keyboard.nextLine();


        List<Movie> matchingMovies;
        MovieDAO myMovieDAOObject;
        try {
            myMovieDAOObject = new JDBCMovieDAO();
            matchingMovies = myMovieDAOObject.getMatchingMovies(userSearchTerm);
        } catch (Exception e) {
            myMovieDAOObject = new FileMovieDAO();
            matchingMovies = myMovieDAOObject.getMatchingMovies(userSearchTerm);
        }

        for (Movie m : matchingMovies) {
            System.out.print(m.getTitle());

            if (!m.getTagline().isEmpty()) {
                System.out.print(" -- " + m.getTagline());
            }

            System.out.println();
        }

    }
}
