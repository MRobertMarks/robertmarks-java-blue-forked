package com.techelevator.projects.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.techelevator.projects.model.Project;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.projects.model.Employee;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JdbcEmployeeDao implements EmployeeDao {

	private final JdbcTemplate jdbcTemplate;

	public JdbcEmployeeDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Employee> getAllEmployees() {
//		return new ArrayList<>();
		List<Employee> employees = new ArrayList<>();
		String sql = "SELECT * FROM employee;";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql);
		while (results.next()) {
			employees.add(mapRowToEmployee(results));
		}
		return employees;
	}

	@Override
	public List<Employee> searchEmployeesByName(String firstNameSearch, String lastNameSearch) {
//		return List.of(new Employee());
		List<Employee> searchEmployees = new ArrayList<>();
		String sql = "SELECT * FROM employee WHERE first_name ILIKE ? AND last_name ILIKE ?;";
		SqlRowSet results =  jdbcTemplate.queryForRowSet(sql, "%" + firstNameSearch + "%", "%" + lastNameSearch + "%");

		while (results.next()){
			searchEmployees.add(mapRowToEmployee(results));
		}
		return searchEmployees;
	}

	@Override
	public List<Employee> getEmployeesByProjectId(Long projectId) {
//		return new ArrayList<>();
		List<Employee> employeesByProjectId = new ArrayList<>();
		String sql = "SELECT * FROM employees " +
				"JOIN project_employee ON employee.employee_id = project_employee.employee_id " +
				"JOIN project ON project.project_id = project_employee.employee_id" +
				"WHERE project.project_id = ?;";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql, projectId);
		while (results.next()) {
			employeesByProjectId.add(mapRowToEmployee(results));
		}
		return employeesByProjectId;
	}

	@Override
	public void addEmployeeToProject(Long projectId, Long employeeId) {
		String sql = "UPDATE project_employee SET project_id = ?, employee_id = ?;";
		jdbcTemplate.update(sql, Long.class, projectId, employeeId);
	}

	@Override
	public void removeEmployeeFromProject(Long projectId, Long employeeId) {
		String sql = "DELETE FROM project_employee WHERE project_id = ? AND employee_id = ?;";
		jdbcTemplate.update(sql, Long.class, projectId, employeeId);
//		sql = "DELETE FROM employee WHERE employee_id = ? RETURNING project_id;";
//		jdbcTemplate.update(sql, );
	}

	@Override
	public List<Employee> getEmployeesWithoutProjects() {
		return new ArrayList<>();
	}

	private Employee mapRowToEmployee(SqlRowSet results) {
		Employee employee = new Employee();
		employee.setId(results.getLong("employee_id"));
		employee.setDepartmentId(results.getLong("department_id"));
		employee.setFirstName(results.getString("first_name"));
		employee.setLastName(results.getString("last_name"));
		if (results.getDate("birth_date") != null) {
			employee.setBirthDate(results.getDate("birth_date").toLocalDate());
		}
		if (results.getDate("hire_date") != null) {
			employee.setHireDate(results.getDate("hire_date").toLocalDate());
		}
		return employee;
	}

}
