package com.techelevator.projects.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.techelevator.projects.model.Project;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;

/*
Step Two: Implement the JdbcDepartmentDao methods
	Complete the data access methods in JdbcDepartmentDao. Refer to the documentation comments in the
	DepartmentDao interface for the expected input and result of each method.
	You can remove any existing return statement in the method when you start working on it.
	After you complete this step, the tests in JdbcDepartmentDaoTests pass.
 */

public class JdbcDepartmentDao implements DepartmentDao {
	
	private final JdbcTemplate jdbcTemplate;

	public JdbcDepartmentDao(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Department getDepartment(Long id) {
		//return new Department(0L, "Not Implemented Yet");
		Department department = null;
		String sql = "SELECT * FROM department WHERE department_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql, id);
		if (results.next()) {
			department = mapRowToDepartment(results);
		}
		return department;
	}

	@Override
	public List<Department> getAllDepartments() {
		// return new ArrayList<>();
		List<Department> departments = new ArrayList<>();
		String sql = "SELECT * FROM department";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql);
		while (results.next()) {
			departments.add(mapRowToDepartment(results));
		}
		return departments;
	}

	@Override
	public void updateDepartment(Department updatedDepartment) {
	String sql = "UPDATE department SET name = ? WHERE department_id = ?";
	jdbcTemplate.update(sql, updatedDepartment.getName(), updatedDepartment.getId());
	}

	private Department mapRowToDepartment(SqlRowSet results) {
		Department department = new Department();
		department.setId(results.getLong("department_id"));
		department.setName(results.getString("name"));
		return department;
	}
}
