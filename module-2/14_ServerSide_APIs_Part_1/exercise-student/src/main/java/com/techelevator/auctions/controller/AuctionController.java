package com.techelevator.auctions.controller;

import com.techelevator.auctions.dao.AuctionDao;
import com.techelevator.auctions.dao.MemoryAuctionDao;
import com.techelevator.auctions.model.Auction;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/auctions")
public class AuctionController {

    private AuctionDao dao;

    public AuctionController() {
        this.dao = new MemoryAuctionDao();
    }
/*
In AuctionController.java, return to the list() action method. Add a String request parameter with
the name title_like. You'll need to make this parameter optional, which means you set a default value
for it in the parameter declaration. In this case, you want to set the default value to an
empty string "".

Look in MemoryAuctionDao.java for a method that returns auctions that have titles containing a
search term. Return that result in the controller method if title_like contains a value, otherwise
return the full list like before.

If completed properly, the searchByTitleShouldReturnList and searchByTitleExpectNone tests pass.
 */
    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<Auction> list(
            @RequestParam(required = false, defaultValue = "") String title_like,
            @RequestParam(required = false, defaultValue = "0") Double currentBid_lte
    ) {
        if ((!title_like.equals("")) && (currentBid_lte != 0)) {
            return dao.searchByTitleAndPrice(title_like, currentBid_lte);
        } else if (!title_like.equals("")) {
            return dao.searchByTitle(title_like);
        } else if (currentBid_lte != 0) {
            return dao.searchByPrice(currentBid_lte);
        } else {
            return dao.list();
        }
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Auction get(@PathVariable int id) {
        return dao.get(id);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public Auction create(@RequestBody Auction auction) {
        return dao.create(auction);
    }

}
