-- 4. Add a "Sports" genre to the genre table. Add the movie "Coach Carter" to the 
-- newly created genre. (1 row each)

-- select *
-- FROM genre
-- join movie_genre on genre.genre_id = movie_genre.genre_id
-- join movie on movie_genre.movie_id = movie.movie_id;

INSERT INTO genre (genre_name)
VALUES ('Sports');

INSERT INTO movie_genre (movie_id, genre_id)
VALUES ((SELECT movie_id FROM movie WHERE title = 'Coach Carter'), (SELECT genre_id FROM genre WHERE genre_name = 'Sports'));


--UPDATE movie
--SET genre_name = 'Sports'
--WHERE movie.title = 'Coach Carter';

