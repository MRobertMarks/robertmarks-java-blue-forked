package com.techelevator;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FindAndReplace {

    public static void main(String[] args) {
        try (Scanner userInput = new Scanner(System.in)) {

            System.out.println("What is the search word?");
            String searchWord = userInput.nextLine();
            if ((searchWord == null) || (searchWord.isEmpty())) {
                System.out.println("The word to replace is empty");
                System.exit(0);
            }

            System.out.println("What is the replacement word?");
            String replacementWord = userInput.nextLine();
            if ((replacementWord == null) || (replacementWord.isEmpty())) {
                System.out.println("The replacement word is empty");
                System.exit(0);
            }

            File sourceFile;
            System.out.println("What is the source file?");
            String path = userInput.nextLine();
            sourceFile = new File(path);
            if (!sourceFile.exists()) {
                System.out.println(path + " does not exist");
                System.exit(0);
            } else if (!sourceFile.isFile()) {
                System.out.println(path + " is not a file");
                System.exit(0);
            }

            File destinationFile;
            System.out.println("What is the destination file?");
            path = userInput.nextLine();
            destinationFile = new File(path);
            // Validate the destination file
            if(path == null || path.isEmpty()) {
                System.out.println("The destination filename is empty");
                System.exit(0);
            }
            if(!destinationFile.getName().endsWith(".txt")) {
                System.out.println("The destination file must end in .txt");
                System.exit(0);
            }

            try (Scanner sourceScanner = new Scanner(sourceFile.getAbsoluteFile());
                 PrintWriter destinationWriter = new PrintWriter(destinationFile.getAbsoluteFile())) {
                while (sourceScanner.hasNextLine()) {
                    String line = sourceScanner.nextLine();
                    destinationWriter.println(line.replaceAll(searchWord, replacementWord));
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }
}
        /*
        Scanner keyboard = new Scanner(System.in);
        System.out.println("What is the name of the file that should be searched?");
        String path = keyboard.nextLine();
        File inputFile = new File(path);
        System.out.println(inputFile.getAbsolutePath());
        System.out.print("What is the search word?");
        String searchWord = keyboard.nextLine();
        System.out.println("Should the search be case sensitive? (Y/N)");
        String caseSensitive = keyboard.nextLine();
        System.out.flush();

        if (!inputFile.exists()) {
            System.out.println(inputFile.getAbsolutePath() + "File not found.");
        } else {
            try {
                Scanner fileScanner = new Scanner(inputFile);
                int x = 0;
                while (fileScanner.hasNextLine()) {
                    String output = fileScanner.nextLine();
                    x++;
                    if (caseSensitive.equalsIgnoreCase("y")) {
                        if (output.contains(searchWord)) {
                            System.out.println(x + ") " + output);
                        }
                    }
                }

            } catch (IOException e) {
                System.out.println("Unable To Create New File!");
                System.out.println(e.getMessage());
                System.out.println("Stack Trace:");
                e.printStackTrace();
            }
        }
    }
}

/*
import com.aspose.words.Document;
import com.aspose.words.FindReplaceOptions;
import com.aspose.words.examples.Utils;

import java.util.regex.Pattern;

public class FindAndReplace {
    private static final String dataDir = Utils.getSharedDataDir(FindAndReplace.class) + "FindAndReplace/";

    public static void main(String[] args) throws Exception {
        //ExStart:
        // The path to the documents directory.
        String dataDir = Utils.getDataDir(FindAndReplace.class);

        // Open the document.
        Document doc = new Document(dataDir + "ReplaceSimple.doc");
        // Check the text of the document
        System.out.println("Original document text: " + doc.getRange().getText());
        Pattern regex = Pattern.compile("_CustomerName_", Pattern.CASE_INSENSITIVE);
        // Replace the text in the document.
        doc.getRange().replace(regex, "James Bond", new FindReplaceOptions());
        // Check the replacement was made.
        System.out.println("Document text after replace: " + doc.getRange().getText());
        // Save the modified document.
        doc.save(dataDir + "ReplaceSimple Out.doc");
        //ExEnd:

        System.out.println("Text found and replaced successfully.");
    }
}
 */

