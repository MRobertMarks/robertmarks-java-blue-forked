package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class FizzWriter {

	public static void main(String[] args) {
		File destinationFile;

		try (Scanner userInput = new Scanner(System.in)) {
			System.out.println("What is the destination file?");
			String path = userInput.nextLine();
			destinationFile = new File(path);
			// Validate the destination file
			if(path == null || path.isEmpty()) {
				System.out.println("The destination filename is empty");
				System.exit(0);
			}
			if(!destinationFile.getName().endsWith(".txt")) {
				System.out.println("The destination file must end in .txt");
				System.exit(0);
			}
		}

		// Write FizzBuzz 1 - 300
		try (PrintWriter destinationWriter = new PrintWriter(destinationFile.getAbsoluteFile())) {
			for (int i = 1; i <= 300; i++) {
				boolean isDivisibleBy5 = i % 5 == 0;
				boolean isDivisibleBy3 = i % 3 == 0;
				if (isDivisibleBy5 && isDivisibleBy3) {
					destinationWriter.println("FizzBuzz");
				} else if (isDivisibleBy3 || Integer.toString(i).contains("3")) {
					destinationWriter.println("Fizz");
				} else if (isDivisibleBy5 || Integer.toString(i).contains("5")) {
					destinationWriter.println("Buzz");
				} else {
					destinationWriter.println(i);
				}
			}

			System.out.println(destinationFile + " has been created.");

		} catch (FileNotFoundException fileNotFoundException) {
			System.out.print(fileNotFoundException.getMessage());
		}
	}

}

		/*
		try (Scanner userInput = new Scanner(System.in)) {
			System.out.println("Destination for FizzBuzz.txt?");
			String filePath = userInput.nextLine();

			writeFileFizzBuzzTo300(filePath);
		}
	}

	private static void writeFileFizzBuzzTo300(String filePath) {
		File fizzBuzzFile = new File(filePath + "/FizzBuzz.txt");

		try (PrintWriter fileOutput = new PrintWriter(fizzBuzzFile)) {
			for (int i = 1; i < 301; i++) {
				if (i % 5 == 0 &&
						i % 3 == 0) {
					fileOutput.println("FizzBuzz");
				} else if (i % 5 == 0 ) {
					fileOutput.println("Buzz");
				} else if (i % 3 == 0 ) {
					fileOutput.println("Fizz");
				} else {
					fileOutput.println(i);
				}
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		}
	}

	private static boolean containsA5(int number) {
		String numberAsString = Integer.toString(number);

		if (numberAsString.contains("5")) {
			return true;
		}
		return false;
	}

	private static boolean containsA3(int number) {
		String numberAsString = Integer.toString(number);

		if (numberAsString.contains("3")) {
			return true;
		}
		return false;

	}

}

*/
