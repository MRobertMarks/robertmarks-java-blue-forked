package com.techelevator;

//transferTo Method

public class BankAccount implements Accountable {

    private String accountHolderName;
    private String accountNumber;
    private int balance;
//    private int transferTo;

    public BankAccount(String accountHolder, String accountNumber) {
        this.accountHolderName = accountHolder;
        this.accountNumber = accountNumber;
        this.balance = 0;
//        this.transferTo = 0;
    }

    public BankAccount(String accountHolder, String accountNumber, int balance) {
        this.accountHolderName = accountHolder;
        this.accountNumber = accountNumber;
        this.balance = balance;
//        this.transferTo = 0;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public int deposit(int amountToDeposit) {
        balance = balance + amountToDeposit;
        return balance;
    }

    public int withdraw(int amountToWithdraw) {
        balance = balance - amountToWithdraw;
        return balance;
    }

  //  public int getTransferTo() {
  //      return transferTo;
  //  }
//
  //  public void setTransferTo(int transferTo) {
  //      this.transferTo = transferTo;
  //  }

    public int transferTo(BankAccount destinationAccount, int transferAmount) {
        this.withdraw(transferAmount);
        destinationAccount.deposit(transferAmount);
        return this.getBalance();
    }
}
