package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class BankCustomer {

    private String name;
    private String address;
    private String phoneNumber;
    public List<Accountable> accounts = new ArrayList<>();
//    private boolean isVip;

  //  public BankCustomer(String name, String address, String phoneNumber, List<Accountable> accounts, boolean isVip) {
  //      this.name = name;
  //      this.address = address;
  //      this.phoneNumber = phoneNumber;
  //      this.accounts = accounts;
  //      this.isVip = isVip;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Accountable[] getAccounts() {
        return accounts.toArray(new Accountable[accounts.size()]);
    }

 //   public void setAccounts(List<Accountable> accounts) {
 //       this.accounts = accounts;
 //   }


    public boolean isVip() {
        int totalMoney = 0;
        for (Accountable totalAccount : accounts) {
            totalMoney = totalAccount.getBalance() + totalMoney;
        }

        if (totalMoney >= 25000) {
            return true;
        }
        return false;
       // return isVip;
    }

 //   public void setVip(boolean vip) {
 //       isVip = vip;
 //   }

    public void addAccount(Accountable newAccount) {
        accounts.add(newAccount);

    }
}
