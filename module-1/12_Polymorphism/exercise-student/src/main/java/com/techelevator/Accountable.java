package com.techelevator;

public interface Accountable {

    int getBalance(); //Returns the balance value of the account in dollars.

}
