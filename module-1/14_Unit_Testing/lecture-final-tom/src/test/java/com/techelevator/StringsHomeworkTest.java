package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StringsHomeworkTest {

    private StringsHomework objectUnderTest;

    @Before
    public void fresh_new_object() {
        objectUnderTest = new StringsHomework();
    }

    @Test
    public void test_stringYak() {
        String input = "yakpak"; // Arrange
        String output = objectUnderTest.stringYak(input); // Act
        Assert.assertEquals("pak", output); // Assert

        String input2 = "pakyak";
        String output2 = objectUnderTest.stringYak(input2);
        Assert.assertEquals("pak", output2);

        String input3 = "yak123ya";
        String output3 = objectUnderTest.stringYak(input3);
        Assert.assertEquals("123ya", output3);
    }

    @Test
    public void test_reverso() {
        String input = "abc"; // Arrange

        String output = objectUnderTest.reverse(input); // Act

        Assert.assertNotNull(output);
        Assert.assertEquals(3, output.length());
        Assert.assertEquals("cba", output); // Assert


        input = "Tom";
        output = objectUnderTest.reverse(input);
        Assert.assertEquals("moT", output);

        input = "";
        output = objectUnderTest.reverse(input);
        Assert.assertEquals("", output);

        input = "abba";
        output = objectUnderTest.reverse(input);
        Assert.assertEquals(input, output);

    }
}
