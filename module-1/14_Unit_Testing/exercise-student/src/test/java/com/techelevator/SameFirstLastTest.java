package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class SameFirstLastTest {

    @Test
    public void one_Two_Three() {
        //Arrange
        SameFirstLast oneTwoThree = new SameFirstLast();
        boolean outputOneTwoThree;
        int[] inputOneTwoThree = new int[]{1, 2, 3};

        //Act
        outputOneTwoThree = oneTwoThree.isItTheSame(inputOneTwoThree);

        //Assert
        Assert.assertFalse(outputOneTwoThree);
    }

    @Test
    public void one_Two_Three_one() {
        //Arrange
        SameFirstLast oneTwoThreeOne = new SameFirstLast();
        boolean outputOneTwoThreeOne;
        int[] inputOneTwoThreeOne = new int[]{1, 2, 3, 1};

        //Act
        outputOneTwoThreeOne = oneTwoThreeOne.isItTheSame(inputOneTwoThreeOne);

        //Assert
        Assert.assertTrue(outputOneTwoThreeOne);
    }
}

/*
Ref
   @Test
    public void output_counts_sheep_words() {

        //Arrange (create new wordcount object)
        WordCount wcSheep = new WordCount();
        String[] inputSheep = new String[] {"ba", "ba", "black", "sheep"};
        Map<String, Integer> outputSheepProgramMap;
        Map<String, Integer> expectedSheepMap = new HashMap<String, Integer>();
        expectedSheepMap.put("ba", 2);
        expectedSheepMap.put("black", 1);
        expectedSheepMap.put("sheep", 1);

        //Act
        outputSheepProgramMap = wcSheep.getCount(inputSheep);

        //Assert
        Assert.assertEquals(expectedSheepMap, outputSheepProgramMap);
    }
 */