package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class MaxEnd3Test {

    @Test
    public void return_all_threes() {

        //Arrange
        MaxEnd3 oneTwoThree = new MaxEnd3();
        int [] inputArray = {1, 2, 3};
        int [] outputExpected = {3, 3, 3};

        //Act
        int [] outputActual = oneTwoThree.makeArray(inputArray);

        //Assert
        Assert.assertArrayEquals(outputExpected, outputActual);
    }

    @Test
    public void return_all_eleven() {

        //Arrange
        MaxEnd3 elevenFiveNine = new MaxEnd3();
        int [] inputArray = {11, 5, 9};
        int [] outputExpected = {11, 11, 11};

        //Act
        int [] outputActual = elevenFiveNine.makeArray(inputArray);

        //Assert
        Assert.assertArrayEquals(outputExpected, outputActual);
    }

    @Test
    public void return_threes_pt2() {

        //Arrange
        MaxEnd3 twoElevenThree = new MaxEnd3();
        int [] inputArray = {2, 11, 3};
        int [] outputExpected = {3, 3, 3};

        //Act
        int [] outputActual = twoElevenThree.makeArray(inputArray);

        //Assert
        Assert.assertArrayEquals(outputExpected, outputActual);
    }
}