package com.techelevator;

import org.junit.Test;
import org.junit.Assert;
import java.util.HashMap;
import java.util.Map;


public class WordCountTest {

    @Test
    public void output_counts_letter_repeats() {

        //Arrange (create new wordcount object)
        WordCount letterCount = new WordCount();
        String[] inputLetter = new String[]{"a", "b", "a", "c", "b"};
        Map<String, Integer> outputLetterProgramMap;
        Map<String, Integer> expectedLetterMap = new HashMap<String, Integer>();
        expectedLetterMap.put("a", 2);
        expectedLetterMap.put("b", 2);
        expectedLetterMap.put("c", 1);

        //Act
        outputLetterProgramMap = letterCount.getCount(inputLetter);

        //Assert
        Assert.assertEquals(expectedLetterMap, outputLetterProgramMap);
    }


    @Test
    public void output_counts_sheep_words() {

        //Arrange (create new wordcount object)
        WordCount wcSheep = new WordCount();
        String[] inputSheep = new String[] {"ba", "ba", "black", "sheep"};
        Map<String, Integer> outputSheepProgramMap;
        Map<String, Integer> expectedSheepMap = new HashMap<String, Integer>();
        expectedSheepMap.put("ba", 2);
        expectedSheepMap.put("black", 1);
        expectedSheepMap.put("sheep", 1);

        //Act
        outputSheepProgramMap = wcSheep.getCount(inputSheep);

        //Assert
        Assert.assertEquals(expectedSheepMap, outputSheepProgramMap);
    }

    @Test
    public void output_blank_words() {

        //Arrange (create new wordcount object)
        WordCount wcBlank = new WordCount();
        String[] inputBlank = new String[] {};
        Map<String, Integer> outputBlankProgramMap;
        Map<String, Integer> expectedBlankMap = new HashMap<String, Integer>();
        //expectedBlankMap.put({});

        //Act
        outputBlankProgramMap = wcBlank.getCount(inputBlank);

        //Assert
        Assert.assertEquals(expectedBlankMap, outputBlankProgramMap);
    }
}


/*
private void helper_method(Map<String, Integer> expected, Map<String, Integer> actual) {
        Assert.assertEquals(expected.size(), actual.size());
        // All keys in the output are in the expected set
        for (String key : actual.keySet()) {
            Assert.assertTrue(expected.containsKey(key));
        }
        // All keys in the expected set are in the output set
        for (String key : expected.keySet()) {
            Assert.assertTrue(actual.containsKey(key));
        }
        // Check values!
        for (String key : actual.keySet()) {
            Assert.assertEquals(expected.get(key), actual.get(key));
        }
    }
 */

/*
Arrange - begin by arranging the conditions of the test, such as setting up test data.
Act - perform the action of interest—meaning, the thing you're testing.
Assert - validate that the expected outcome occurred by means of an assertion—that is, a
certain value was returned, or a file exists.
#
 */

/*
Naming matters
Test methods must clearly state what's being tested in the method name.

For instance, if you want to verify that an Add method returns 4 when it's passed 2 and 2, then the name of the test method should be something like add_should_return_4_when_2_and_2_are_passed. This is verbose, but verbose methods are preferred in unit tests, as they clearly articulate what's being tested.
 */