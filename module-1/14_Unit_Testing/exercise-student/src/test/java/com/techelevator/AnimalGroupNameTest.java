package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class AnimalGroupNameTest {

    @Test
    public void typo_returns_unknown() {

        //Arrange
        AnimalGroupName typoUnknown = new AnimalGroupName();
        // String unknownExpected = "Crash"; (unnecessary since exact expected string is in assertion statement, but which is the best practice?)

        //Act
        String typoActual = typoUnknown.getHerd("effarig");

        //Assert
        Assert.assertEquals("unknown", typoActual);
    }

    @Test
    public void rhino_returns_crash() {

        //Arrange
        AnimalGroupName rhinoCrash = new AnimalGroupName();
        String crashExpected = "Crash";

        //Act
        String rhinoHerdActual = rhinoCrash.getHerd("rhino");

        //Assert
        Assert.assertEquals(crashExpected, rhinoHerdActual);
    }
    @Test
    public void rhino_caps_returns_crash() {

        //Arrange
        AnimalGroupName rhinoCapsCrash = new AnimalGroupName();
        String crashCapsExpected = "Crash";

        //Act
        String rhinoCapsHerdActual = rhinoCapsCrash.getHerd("RHINO");

        //Assert
        Assert.assertEquals(crashCapsExpected, rhinoCapsHerdActual);
    }

    @Test
    public void blank_returns_unknown() {

        //Arrange
        AnimalGroupName blankUnknown = new AnimalGroupName();
        String blankUnknownExpected = "unknown";

        //Act
        String blankUnknownHerdActual = blankUnknown.getHerd(" ");

        //Assert
        Assert.assertEquals(blankUnknownExpected, blankUnknownHerdActual);
    }
}
