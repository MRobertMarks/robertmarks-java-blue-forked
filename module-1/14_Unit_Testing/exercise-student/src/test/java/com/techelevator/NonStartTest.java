package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class NonStartTest {
    @Test
    public void ello_here_test() {
        //Arrange
        NonStart hiThere = new NonStart();
        String actualOutput;
        String shouldOutput = "ellohere";

        //Act
        actualOutput = hiThere.getPartialString("Hello", "There");

        //Assert
        Assert.assertEquals(shouldOutput, actualOutput);
    }
    @Test
    public void java_shot_test() {
        //Arrange
        NonStart hotJava = new NonStart();
        String actualOutput;
        String shouldOutput = "hotlava";

        //Act
        actualOutput = hotJava.getPartialString("shotl", "java");

        //Assert
        Assert.assertEquals(shouldOutput, actualOutput);
    }

    @Test
    public void zero_test() {
        //Arrange
        NonStart hotJava = new NonStart();
        String actualOutput;
        String shouldOutput = "";

        //Act
        actualOutput = hotJava.getPartialString("", "1");

        //Assert
        Assert.assertEquals(shouldOutput, actualOutput);
    }


}
