package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class CigarPartyTest {

    @Test
    public void  fifty_party_false_test() {
        //Arrange
        CigarParty fiftyFalse = new CigarParty();
        boolean outputFiftyFalse;
        //inputFiftyFalse = haveParty(30, false);

        //Act
        outputFiftyFalse = fiftyFalse.haveParty(50, false);

        //Assert
        Assert.assertEquals(true, outputFiftyFalse);
    }


    @Test
    public void  thirty_party_false_test() {
        //Arrange
        CigarParty thirtyFalse = new CigarParty();
        boolean outputThirtyFalse;
        //inputThirtyFalse = haveParty(30, false);

        //Act
        outputThirtyFalse = thirtyFalse.haveParty(30, false);

        //Assert
        Assert.assertEquals(false, outputThirtyFalse);
    }
    @Test
    public void  seventy_party_true_test() {
        //Arrange
        CigarParty seventyTrue = new CigarParty();
        boolean outputSeventyTrue;
        //inputFiftyFalse = haveParty(30, false);

        //Act
        outputSeventyTrue = seventyTrue.haveParty(50, false);

        //Assert
        Assert.assertTrue(outputSeventyTrue);
    }
}


/*
Personal Ref

import org.junit.Assert;
import org.junit.Test;

public class SameFirstLastTest {

    @Test
    public void one_Two_Three() {
        //Arrange
        SameFirstLast oneTwoThree = new SameFirstLast();
        boolean outputOneTwoThree;
        int[] inputOneTwoThree = new int[]{1, 2, 3};

        //Act
        outputOneTwoThree = oneTwoThree.isItTheSame(inputOneTwoThree);

        //Assert
        Assert.assertEquals(false, outputOneTwoThree);
    }
 */