package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class StringBitsTest {

    @Test
    public void hello_is_hlo() {
        //Arrange
        StringBits hello = new StringBits();
        String outputActual;
        String outputExpected = "Hlo";

        //Act
        outputActual = hello.getBits("Hello");

        //Assert
        Assert.assertEquals(outputExpected, outputActual);
    }

    @Test
    public void hi_is_h() {
        //Arrange
        StringBits hiH = new StringBits();
        String outputActual;
        String outputExpected = "H";

        //Act
        outputActual = hiH.getBits("Hi");

        //Assert
        Assert.assertEquals(outputExpected, outputActual);
    }

    @Test
    public void heeololeo_is_hello() {
        //Arrange
        StringBits Heeololeo = new StringBits();
        String outputActual;
        String outputExpected = "Hello";

        //Act
        outputActual = Heeololeo.getBits("Heeololeo");

        //Assert
        Assert.assertEquals(outputExpected, outputActual);
    }
}
