package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class Lucky13Test {
    @Test
    public void zer0_2wo_4our_2rue() {

        //Arrange
        Lucky13 noOneThree = new Lucky13();
        // boolean output; (this seems unnecessary if declared in the Act?)

        //Act - please explain new int in the expression ".getLucky(new int[] {0, 2, 4});"
        boolean outputActual = noOneThree.getLucky(new int[]{0, 2, 4});

        //Assert
        Assert.assertTrue(outputActual);
    }

    @Test
    public void false_124() {

        //Arrange
        Lucky13 oneTwoFourFalse = new Lucky13();
        boolean outputActual;

        //act
        outputActual = oneTwoFourFalse.getLucky(new int[]{1, 2, 4});

        //Assert
        Assert.assertFalse(outputActual);
    }

    @Test
    public void false_123() {

        //Arrange
        Lucky13 oneTwoThreeFalse = new Lucky13();
        boolean outputActual;

        //act
        outputActual = oneTwoThreeFalse.getLucky(new int[]{1, 2, 3});

        //Assert
        Assert.assertFalse(outputActual);
    }

    @Test
    public void false_13() {

        //Arrange
        Lucky13 oneThreeFalse = new Lucky13();
        boolean outputActual;

        //act
        outputActual = oneThreeFalse.getLucky(new int[]{1, 3});

        //Assert
        Assert.assertFalse(outputActual);
    }

    @Test
    public void false_126() {

        //Arrange
        Lucky13 oneTwoSixFalse = new Lucky13();
        boolean outputActual;

        //act
        outputActual = oneTwoSixFalse.getLucky(new int[]{1, 2, 3, 4, 5, 6,});

        //Assert
        Assert.assertFalse(outputActual);
    }

    @Test
    public void true_26() {

        //Arrange
        Lucky13 twoSixTrue = new Lucky13();
        boolean outputActual;

        //act
        outputActual = twoSixTrue.getLucky(new int[]{2, 4, 5, 6,});

        //Assert
        Assert.assertTrue(outputActual);
    }

//   @Test
//   public void null_test() {

//       //Arrange
//       Lucky13 nullTest = new Lucky13();
//       boolean outputActual;

//       //act
//       outputActual = nullTest.getLucky(new int[]{});

//       //Assert
//       Assert.assertFalse(outputActual);
//   }
}