package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class FrontTimesTest {

    @Test
    public void front_times_two() {
        //Arrange
        FrontTimes frontTwo = new FrontTimes();
        String resultTwoActual;
        String resultTwoExpected = "ChoCho";


        //Act
        resultTwoActual = frontTwo.generateString("Chocolate", 2);

        //Assert
        Assert.assertEquals(resultTwoExpected, resultTwoActual);

    }
    @Test
    public void front_times_three() {
        //Arrange
        FrontTimes frontThree = new FrontTimes();
        String resultThreeActual;
        String resultThreeExpected = "ChoChoCho";


        //Act
        resultThreeActual = frontThree.generateString("Chocolate", 3);

        //Assert
        Assert.assertEquals(resultThreeExpected, resultThreeActual);

    }
    @Test
    public void front_times_null() {
        //Arrange
        FrontTimes frontNull = new FrontTimes();
        String resultNullActual;
        String resultNullExpected = "";


        //Act
        resultNullActual = frontNull.generateString("", 2);

        //Assert
        Assert.assertEquals(resultNullExpected, resultNullActual);

    }

}
