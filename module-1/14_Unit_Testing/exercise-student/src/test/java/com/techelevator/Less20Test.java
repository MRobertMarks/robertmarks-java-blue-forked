package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class Less20Test {

    @Test
    public void fourty_test() {
        //Arrange
        Less20 tryFourty = new Less20();

        //Act
        boolean outputFourty = tryFourty.isLessThanMultipleOf20(40);

        //Assert
        Assert.assertFalse(outputFourty);
    }

    @Test
    public void eighteen_test() {
        //Arrange
        Less20 tryEighteen = new Less20();
        int inputEighteen = 18;

        //Act
        boolean outputEighteen = tryEighteen.isLessThanMultipleOf20(inputEighteen);

        //Assert
        Assert.assertTrue(outputEighteen);
    }



}
