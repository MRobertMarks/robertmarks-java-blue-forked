package com.techelevator;

import org.junit.Assert;
import org.junit.Test;

public class DateFashionTest {
    @Test
    public void date_fashion_2() {
        //Arrange
        DateFashion dateTwo = new DateFashion();
        int expectResult = 2;

        //Act
        int dateResult = dateTwo.getATable(5, 10);

        //Assert
        Assert.assertEquals(expectResult, dateResult);
    }
    @Test
    public void date_fashion_0() {
        //Arrange
        DateFashion dateZero = new DateFashion();
        int expectResult = 0;

        //Act
        int dateResult = dateZero.getATable(5, 2);

        //Assert
        Assert.assertEquals(expectResult, dateResult);
    }
}
