package com.techelevator.challenge;


import org.junit.BeforeClass;

import org.junit.Test;


import com.techelevator.SafeReflection;


import java.lang.reflect.*;

import java.math.BigDecimal;


import static org.junit.Assert.*;


public class BankAccountTests extends com.techelevator.BankAccountTests {


    private static Class account;

    private static final String FULLY_QUALIFIED_CLASS_NAME = "com.techelevator.challenge.BankAccount";


    @BeforeClass

    public static void classShouldExist() {

        try {

            account = Class.forName(FULLY_QUALIFIED_CLASS_NAME);

        } catch (Exception e) {

            fail(FULLY_QUALIFIED_CLASS_NAME + " class does not exist");

        }

    }



    @Override

    @Test

    public void shouldContainFieldBalance() {

        Field f = SafeReflection.getDeclaredField(account, "balance");

        assertNotNull("Field balance does not exist",f);

        assertEquals("Field balance should be of Type BigDecimal", BigDecimal.class.getTypeName(), f.getType().getName());

    }


    @Override

    @Test

    public void bankAccountHasTwoArgsConstructor() throws IllegalAccessException, InvocationTargetException, InstantiationException {

        Constructor constructor = SafeReflection.getConstructor(account,String.class, String.class);

        assertNotNull(FULLY_QUALIFIED_CLASS_NAME + " should contain a 2 argument constructor that sets account holder name and number.",constructor);

    }


    @Override

    @Test

    public void bankAccountHasThreeArgsConstructor() {

        Constructor constructor = SafeReflection.getConstructor(account,String.class, String.class, BigDecimal.class);

        assertNotNull(FULLY_QUALIFIED_CLASS_NAME + " should contain a 3 argument constructor that sets account holder name, number and balance.",constructor);

    }



    @Override

    @Test

    public void twoArgumentConstructorShouldSetNameAndNumber() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(account,String.class, String.class);

        assertNotNull(FULLY_QUALIFIED_CLASS_NAME + " should contain a 2 argument constructor that sets account holder name and number.",constructor);


        Object bankAccount = constructor.newInstance("John Smith", "CHK:1234");


        Method getAccountNameHolder = bankAccount.getClass().getMethod("getAccountHolderName");

        assertEquals("John Smith", getAccountNameHolder.invoke(bankAccount));


        Method getAccountNumber = bankAccount.getClass().getMethod("getAccountNumber");

        assertEquals("CHK:1234", getAccountNumber.invoke(bankAccount));


        Method getBalance = bankAccount.getClass().getMethod("getBalance");



        assertEquals(BigDecimal.ZERO, getBalance.invoke(bankAccount));

    }


    @Override

    @Test

    public void newCustomerHasZeroBalance() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(account,String.class, String.class);

        Object bankAccount = constructor.newInstance("","");

        Method getBalance = bankAccount.getClass().getMethod("getBalance");



        assertEquals(BigDecimal.ZERO, getBalance.invoke(bankAccount));

    }


    @Override

    @Test

    public void threeArgumentConstructorShouldSetNameNumberAndBalance() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(account,String.class, String.class, BigDecimal.class);



        assertNotNull(FULLY_QUALIFIED_CLASS_NAME + " should contain a 3 argument constructor that sets account holder name, number and balance.",constructor);


        Object bankAccount = constructor.newInstance("John Smith","CHK:1234", BigDecimal.ONE);

        Method getAccountNameHolder = bankAccount.getClass().getMethod("getAccountHolderName");

        assertEquals("John Smith", getAccountNameHolder.invoke(bankAccount));


        Method getAccountNumber = bankAccount.getClass().getMethod("getAccountNumber");

        assertEquals("CHK:1234", getAccountNumber.invoke(bankAccount));


        Method getBalance = bankAccount.getClass().getMethod("getBalance");

        assertEquals(BigDecimal.ONE, getBalance.invoke(bankAccount));

    }


    @Override

    @Test

    public void accountHolderNameShouldOnlyHaveAGetter() {

        assertTrue("Account Holder Name field should have a getter",SafeReflection.hasGetter(account, "AccountHolderName"));

        assertFalse("Account Holder Name field should not have a setter",SafeReflection.hasSetter(account, "AccountHolderName"));

    }


    @Override

    @Test

    public void accountNumberShouldOnlyHaveAGetter() throws NoSuchMethodException {

        assertTrue("Account Number field should have a getter",SafeReflection.hasGetter(account, "AccountNumber"));

        assertFalse("Account Number field should not have a setter",SafeReflection.hasSetter(account, "AccountNumber"));

    }


    @Override

    @Test

    public void balanceShouldOnlyHaveAGetter() throws NoSuchMethodException {

        assertTrue("Balance field should have a getter",SafeReflection.hasGetter(account, "Balance"));

        assertFalse("Balance field should not have a setter method",SafeReflection.hasSetter(account, "Balance"));

    }


    @Override

    @Test

    public void depositIncreasesBalance() throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(account,String.class, String.class);

        Object bankAccount = constructor.newInstance("","");



        Method deposit = bankAccount.getClass().getMethod("deposit", BigDecimal.class);

        assertEquals(BigDecimal.ONE, deposit.invoke(bankAccount, BigDecimal.ONE));



        Method getBalance = bankAccount.getClass().getMethod("getBalance");

        assertEquals(BigDecimal.ONE, getBalance.invoke(bankAccount));

    }


    @Override

    @Test

    public void withdrawDecreasesBalance() throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(account,String.class, String.class);

        Object bankAccount = constructor.newInstance("","");


        Method withdraw = bankAccount.getClass().getMethod("withdraw", BigDecimal.class);

        assertEquals(BigDecimal.valueOf(-1), withdraw.invoke(bankAccount, BigDecimal.ONE));



        Method getBalance = bankAccount.getClass().getMethod("getBalance");

        assertEquals(BigDecimal.valueOf(-1), getBalance.invoke(bankAccount));



    }

}