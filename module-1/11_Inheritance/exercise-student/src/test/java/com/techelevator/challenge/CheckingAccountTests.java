package com.techelevator.challenge;


import static org.junit.Assert.*;


import org.junit.BeforeClass;

import org.junit.Test;


import com.techelevator.SafeReflection;


import java.lang.reflect.Constructor;

import java.lang.reflect.InvocationTargetException;

import java.lang.reflect.Method;

import java.math.BigDecimal;


public class CheckingAccountTests extends com.techelevator.CheckingAccountTests {


    private static Class chkAccount;

    private static final String FULLY_QUALIFIED_CLASS_NAME = "com.techelevator.challenge.CheckingAccount";


    @BeforeClass

    public static void classShouldExist() {

        try {

            chkAccount = Class.forName(FULLY_QUALIFIED_CLASS_NAME);

        } catch (Exception e) {

            fail(FULLY_QUALIFIED_CLASS_NAME + " class does not exist");

        }

    }



    @Override

    @Test

    public void shouldHaveThreeArgumentConstructor() {

        Constructor constructor = SafeReflection.getConstructor(chkAccount,String.class,String.class,BigDecimal.class);



        assertNotNull("com.techelevator.CheckingAccount should contain a 3 argument constructor that calls the matching BankAccount constructor.",constructor);

    }


    @Override

    @Test

    public void withdrawNegativeWithFeeBalance_Test() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(chkAccount,String.class,String.class,BigDecimal.class);

        Object checkingAccount = constructor.newInstance("", "", BigDecimal.valueOf(-1));



        Method withdraw = checkingAccount.getClass().getMethod("withdraw", BigDecimal.class);

        Method getBalance = checkingAccount.getClass().getMethod("getBalance");

        BigDecimal newBalance = (BigDecimal) withdraw.invoke(checkingAccount, BigDecimal.ONE);

        assertEquals(BigDecimal.valueOf(-12), newBalance);

        assertEquals(BigDecimal.valueOf(-12), getBalance.invoke(checkingAccount));

    }



    @Override

    @Test

    public void withdrawPositiveWithFee_Test() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(chkAccount,String.class,String.class,BigDecimal.class);

        Object checkingAccount = constructor.newInstance("", "", BigDecimal.valueOf(-1));



        Method withdraw = checkingAccount.getClass().getMethod("withdraw", BigDecimal.class);

        Method getBalance = checkingAccount.getClass().getMethod("getBalance");

        BigDecimal newBalance = (BigDecimal) withdraw.invoke(checkingAccount, BigDecimal.valueOf(2));

        assertEquals(BigDecimal.valueOf(-13), newBalance);

        assertEquals(BigDecimal.valueOf(-13), getBalance.invoke(checkingAccount));

    }


    @Override

    @Test

    public void withdrawNegativeBalanceBelow100_Test() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(chkAccount,String.class,String.class,BigDecimal.class);

        Object checkingAccount = constructor.newInstance("", "", BigDecimal.valueOf(-100));



        Method withdraw = checkingAccount.getClass().getMethod("withdraw", BigDecimal.class);

        Method getBalance = checkingAccount.getClass().getMethod("getBalance");

        BigDecimal newBalance = (BigDecimal) withdraw.invoke(checkingAccount, BigDecimal.valueOf(2));

        assertEquals(BigDecimal.valueOf(-100), newBalance);

        assertEquals(BigDecimal.valueOf(-100), getBalance.invoke(checkingAccount));

    }


    @Override

    @Test

    public void withdrawPositiveBalance_Test() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(chkAccount,String.class,String.class,BigDecimal.class);

        Object checkingAccount = constructor.newInstance("", "", BigDecimal.valueOf(10));



        Method withdraw = checkingAccount.getClass().getMethod("withdraw", BigDecimal.class);

        Method getBalance = checkingAccount.getClass().getMethod("getBalance");

        BigDecimal newBalance = (BigDecimal) withdraw.invoke(checkingAccount, BigDecimal.valueOf(5));

        assertEquals(BigDecimal.valueOf(5), newBalance);

        assertEquals(BigDecimal.valueOf(5), getBalance.invoke(checkingAccount));

    }

}