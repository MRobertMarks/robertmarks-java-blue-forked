package com.techelevator.challenge;


import static org.junit.Assert.*;


import org.junit.BeforeClass;

import org.junit.Test;


import com.techelevator.SafeReflection;


import java.lang.reflect.Constructor;

import java.lang.reflect.InvocationTargetException;

import java.lang.reflect.Method;

import java.math.BigDecimal;


public class SavingsAccountTests extends com.techelevator.SavingsAccountTests {


    private static Class savAccount;

    private static final String FULLY_QUALIFIED_CLASS_NAME = "com.techelevator.challenge.SavingsAccount";


    @BeforeClass

    public static void classShouldExist() {

        try {

            savAccount = Class.forName(FULLY_QUALIFIED_CLASS_NAME);

        } catch (Exception e) {

            fail(FULLY_QUALIFIED_CLASS_NAME + " class does not exist");

        }

    }


    @Override

    @Test

    public void shouldHaveThreeArgumentConstructor() {

        Constructor constructor = SafeReflection.getConstructor(savAccount,String.class,String.class,BigDecimal.class);



        assertNotNull(FULLY_QUALIFIED_CLASS_NAME + " should contain a 3 argument constructor that calls the matching BankAccount constructor.",constructor);

    }


    @Override

    @Test

    public void tryToWithdrawFromNegativeBalance() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(savAccount,String.class,String.class,BigDecimal.class);

        Object savingsAccount = constructor.newInstance("", "", BigDecimal.valueOf(-1));

        Method withdraw = savingsAccount.getClass().getMethod("withdraw", BigDecimal.class);

        Method getBalance = savingsAccount.getClass().getMethod("getBalance");

        BigDecimal newBalance = (BigDecimal) withdraw.invoke(savingsAccount, BigDecimal.ONE);

        assertEquals(BigDecimal.valueOf(-1), newBalance);

        assertEquals(BigDecimal.valueOf(-1), getBalance.invoke(savingsAccount));

    }


    @Override

    @Test

    public void sendPositiveIntoNegativeTest() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(savAccount,String.class,String.class,BigDecimal.class);

        Object savingsAccount = constructor.newInstance("", "", BigDecimal.ONE);

        Method withdraw = savingsAccount.getClass().getMethod("withdraw", BigDecimal.class);

        Method getBalance = savingsAccount.getClass().getMethod("getBalance");

        BigDecimal newBalance = (BigDecimal) withdraw.invoke(savingsAccount, BigDecimal.valueOf(-2));

        assertEquals(BigDecimal.ONE, newBalance);

        assertEquals(BigDecimal.ONE, getBalance.invoke(savingsAccount));

    }


    @Override

    @Test

    public void tryToWithdrawFromPositiveBalance() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Constructor constructor = SafeReflection.getConstructor(savAccount,String.class,String.class,BigDecimal.class);

        Object savingsAccount = constructor.newInstance("", "", BigDecimal.valueOf(200));

        Method withdraw = savingsAccount.getClass().getMethod("withdraw", BigDecimal.class);

        Method getBalance = savingsAccount.getClass().getMethod("getBalance");

        BigDecimal newBalance = (BigDecimal) withdraw.invoke(savingsAccount, BigDecimal.valueOf(10));

        assertEquals(BigDecimal.valueOf(190), newBalance);

        assertEquals(BigDecimal.valueOf(190), getBalance.invoke(savingsAccount));

    }


    @Override

    @Test

    public void tryToWithdrawFromBalanceBelow150() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException {

        Constructor constructor = SafeReflection.getConstructor(savAccount,String.class,String.class,BigDecimal.class);

        Object savingsAccount = constructor.newInstance("", "", BigDecimal.valueOf(151));

        Method withdraw = savingsAccount.getClass().getMethod("withdraw", BigDecimal.class);

        Method getBalance = savingsAccount.getClass().getMethod("getBalance");

        BigDecimal newBalance = (BigDecimal) withdraw.invoke(savingsAccount, BigDecimal.valueOf(10));

        assertEquals(BigDecimal.valueOf(139), newBalance);

        assertEquals(BigDecimal.valueOf(139), getBalance.invoke(savingsAccount));

    }

}