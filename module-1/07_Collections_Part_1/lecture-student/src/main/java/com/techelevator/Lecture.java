package com.techelevator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Lecture {

	public static void main(String[] args) {
		System.out.println("####################");
		System.out.println("       LISTS");
		System.out.println("####################");

		//Declaration
		//List<String> instructors;
		//Instantiate a list object - no initialization data
		//instructors = new ArrayList<String>(); //creates empty List on heap

		//Concat the above
		List<String> instructors = new ArrayList<String>();

		instructors.size(); //asks 'what's in here'
		instructors.add("Tom"); //adds the data //size: 1
		instructors.add("Tom"); //size:2
		instructors.add("beth"); //size: 3
		instructors.add("walt"); //size: 4


		System.out.println("####################");
		System.out.println("Lists are ordered");
		System.out.println("####################");

		for (int i = 0; i < instructors.size(); i++) {
			System.out.println(instructors.get(i));

		}

		System.out.println("####################");
		System.out.println("Lists allow duplicates");
		System.out.println("####################");

		System.out.println("####################");
		System.out.println("Lists allow elements to be inserted in the middle");
		System.out.println("####################");

		instructors.add(1, "Andrew");

		for (int i = 0; i < instructors.size(); i++) {
			System.out.println(instructors.get(i));
		}

		System.out.println("####################");
		System.out.println("Lists allow elements to be removed by index");
		System.out.println("####################");

		instructors.remove(0);

		for (int i = 0; i < instructors.size(); i++) {
			System.out.println(instructors.get(i));
		}


		System.out.println("####################");
		System.out.println("Find out if something is already in the List");
		System.out.println("####################");

		instructors.add("Tom");

		boolean hasBeth = instructors.contains("Beth");

		System.out.println("List contains Beth?" + hasBeth);


		System.out.println("####################");
		System.out.println("Find index of item in List");
		System.out.println("####################");

		int tomIndex = instructors.indexOf("Tom");
		//will stop at 1st tome, not listing all indexes containing time
		int lastTomIndex = instructors.lastIndexOf("Tom");
		//will list and stop at only the last index containing Tom

		System.out.println("First Tom located at" + tomIndex);
		System.out.println("Last Tom indexed at" + lastTomIndex);


		System.out.println("####################");
		System.out.println("Lists can be turned into an array");
		System.out.println("####################");

		String[] arr = instructors.toArray(new String[instructors.size()]);
		System.out.println(arr[0]);
		System.out.println(arr[3]);
		System.out.println(arr[1]);

		System.out.println("####################");
		System.out.println("Lists can be sorted");
		System.out.println("####################");

		Collections.sort(instructors);

		for (int i = 0; i < instructors.size(); i++) {
			System.out.print("Index #" + i + ": ");
			System.out.println(instructors.get(i));
		}

		System.out.println("####################");
		System.out.println("Lists can be reversed too");
		System.out.println("####################");

		Collections.reverse(instructors);

		for (int i = 0; i < instructors.size(); i++) {
			System.out.print("Index #" + i + ": ");
			System.out.println(instructors.get(i));
			//instructors.remove(i);
		}

			System.out.println("####################");
			System.out.println("       FOREACH");
			System.out.println("####################");
			System.out.println();

			for (String instructor : instructors) {
				System.out.println("Currently, the 'instructor' variable is equal to: " + instructor);
			}

		}
}
