package com.techelevator;

import java.io.File ;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.SortedMap;

public class WordSearch {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		System.out.println("What is the fully qualified name of the file that should be searched?");
		String path = keyboard.nextLine();
		File inputFile = new File(path);
		System.out.println(inputFile.getAbsolutePath());
		System.out.print("What is the search word you are looking for?");
		String searchWord = keyboard.nextLine();
		System.out.println("Should the search be case sensitive? (Y/N)");
		String caseSensitive = keyboard.nextLine();
		System.out.flush();

		if (!inputFile.exists()) {
			System.out.println(inputFile.getAbsolutePath() + "File not found.");
		} else {
			try {
				Scanner fileScanner = new Scanner(inputFile);
				int x = 0;
				while (fileScanner.hasNextLine()) {
					String output = fileScanner.nextLine();
					x++;
					if (caseSensitive.equalsIgnoreCase("y")) {
						if (output.contains(searchWord)) {
							System.out.println(x + ") " + output);
						}
					}
					if (caseSensitive.equalsIgnoreCase("n")) {
						String output2 = output.toLowerCase();
						if (output2.contains(searchWord.toLowerCase())) {
							System.out.println(x + ") " + output);
						}
					}
				}
			} catch (IOException e) {
				System.out.println("Unable To Create New File!");
				System.out.println(e.getMessage());
				System.out.println("Stack Trace:");
				e.printStackTrace();
			}
		}
	}
}


/*
Reference

		//System.out.println(inputFile.exists());
		//String outputFinal = output.(searchWord);
		File inputFile = getInputFileFromUser();
		String searchTerm = getSearchTermFromUser();
		boolean caseSensitive = caseSensitive();
		int lineNumber = 0;

		try (Scanner fileScanner = new Scanner(inputFile)) {
			while (fileScanner.hasNextLine()) {
			String line = fileScanner.nextLine();
			lineNumber++;
				if (caseSensitive == true && line.contains(searchTerm)) {
				System.out.println(lineNumber + ") " + line);
				System.out.println();
				} else if (caseSensitive == false && line.toLowerCase().contains(searchTerm.toLowerCase())) {
	 				System.out.println(lineNumber + ") " + line);
					System.out.println();
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("The file was not found. Please enter a valid file path.");
		}
	}

	private static Scanner userInput = new Scanner(System.in);

	private static File getInputFileFromUser() {
				System.out.println("What is the fully qualified name of the file that should be searched?");
				String path = userInput.nextLine();
				File inputFile = new File(path);
				if (inputFile.exists() == false) {
					System.out.println(path + " does not exist");
					System.exit(1);
				} else if (inputFile.isFile() == false) {
					System.out.println(path + " is not a file");
					System.exit(1);
				}
				return inputFile;
			}
			private static String getSearchTermFromUser() {
				System.out.println("What is the search word you are looking for?");
				String searchTerm = userInput.nextLine();
				if (searchTerm.equals("") || searchTerm == null) {
					System.out.println("Please enter the word you would like to search for");
					System.exit(1);
				}
				return searchTerm;
			}
			private static boolean caseSensitive() {
				boolean caseSensitive = false;
				System.out.println("Should the search be case sensitive? (Y\\N)");
				String userAnswer = userInput.nextLine();
				if (!userAnswer.equals("Y") && !userAnswer.equals("y") && !userAnswer.equals("N") && !userAnswer.equals("n")) {
					System.out.println("Invalid response. Please enter Y or N.");
					System.exit(1);
				} else if (userAnswer.equals("Y") || userAnswer.equals("y")) {
					caseSensitive = true;
				}
				return caseSensitive;
			}
		}

 */


/*	public static void main(String[] args) throws FileNotFoundException {

		printApplicationBanner();

		File inputFile = getInputFileFromUser();
		try(Scanner fileScanner = new Scanner(inputFile)) {
			while(fileScanner.hasNextLine()) {
				String line = fileScanner.nextLine();
				String rtn = line.substring(0, 9);

				if(checksumIsValid(rtn) == false) {
					System.out.println(line);
				}
			}
		}
	}

 */

/*
Ashley Necciai5:30 PM
public class WordSearch {

    public static void main(String[] args){
        Scanner fileScanner = new Scanner(System.in);
        File inputFile = getInputFileFromUser(fileScanner);
        String searchTerm = getSearchTermFromUser(fileScanner);
        boolean caseSensitive = caseSensitive(fileScanner);
        int lineNumber = 0;
        try (while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                lineNumber++;
                if (caseSensit

                Brandon Grumski5:32 PM
try (Scanner fileScanner = new Scanner(inputFile)) {

			while (fileScanner.hasNextLine()) {
 */

/*
public class WordSearch {
    public static void main(String[] args){
        File inputFile = getInputFileFromUser();
        String searchTerm = getSearchTermFromUser();
        boolean caseSensitive = caseSensitive();
        int lineNumber = 0;
        try (Scanner fileScanner = new Scanner(inputFile)) {
            while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                lineNumber++;
                if (caseSensitive == true && line.contains(searchTerm)) {
                    System.out.println(lineNumber + ") " + line);
                    System.out.println();
                } else if (caseSensitive == false && line.toLowerCase().contains(searchTerm.toLowerCase())) {
                    System.out.println(lineNumber + ") " + line);
                    System.out.println();
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file was not found. Please enter a valid file path.");
        }
    }
    private static Scanner userInput = new Scanner(System.in);
    private static File getInputFileFromUser() {
        System.out.print("What is the fully qualified name of the file that should be searched?");
        String path = userInput.nextLine();
        File inputFile = new File(path);
        if (inputFile.exists() == false) {
            System.out.println(path + " does not exist");
            System.exit(1);
        } else if (inputFile.isFile() == false) {
            System.out.println(path + " is not a file");
            System.exit(1);
        }
        return inputFile;
    }
    private static String getSearchTermFromUser() {
        System.out.println("What is the search word you are looking for?");
        String searchTerm = userInput.nextLine();
        if (searchTerm.equals("") || searchTerm == null) {
            System.out.println("Please enter the word you would like to search for");
            System.exit(1);
        }
        return searchTerm;
    }
    private static boolean caseSensitive() {
        boolean caseSensitive = false;
        System.out.println("Should the search be case sensitive? (Y\\N)");
        String userAnswer = userInput.nextLine();
        if (!userAnswer.equals("Y") && !userAnswer.equals("y") && !userAnswer.equals("N") && !userAnswer.equals("n")) {
            System.out.println("Invalid response. Please enter Y or N.");
            System.exit(1);
        } else if (userAnswer.equals("Y") || userAnswer.equals("y")) {
            caseSensitive = true;
        }
        return caseSensitive;
    }
}
 */