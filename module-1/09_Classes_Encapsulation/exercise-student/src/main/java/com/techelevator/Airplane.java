package com.techelevator;

public class Airplane {

    private String planeNumber;
    private int totalFirstClassSeats;
    private int bookedFirstClassSeats;
    private int totalCoachSeats;
    private int bookedCoachSeats;

    public Airplane(String planeNumber, int totalFirstClassSeats, int totalCoachSeats) {
        this.planeNumber = planeNumber;
        this.totalFirstClassSeats = totalFirstClassSeats;
        this.totalCoachSeats = totalCoachSeats;
    }

    public String getPlaneNumber() {
        return planeNumber;
    }

    public int getBookedFirstClassSeats() {
        return bookedFirstClassSeats;
    }

    public int getAvailableFirstClassSeats() {
        return totalFirstClassSeats - bookedFirstClassSeats;
    }

    public int getTotalFirstClassSeats() {
        return totalFirstClassSeats;
    }

    public int getBookedCoachSeats() {
        return bookedCoachSeats;
    }

    public int getAvailableCoachSeats() {
        return totalCoachSeats - bookedCoachSeats;
    }

    public int getTotalCoachSeats() {
        return totalCoachSeats;
    }

    public boolean reserveSeats(boolean firstClass, int totalNumberOfSeats) {
        if (firstClass) {
            if (totalNumberOfSeats > getAvailableFirstClassSeats()) {
                return false;
            }
            bookedFirstClassSeats += totalNumberOfSeats;
        } else {
            if (totalNumberOfSeats > getAvailableCoachSeats()) {
                return false;
            }
            bookedCoachSeats += totalNumberOfSeats;
        }
        return true;
    }
}


/*
Brandon
public boolean reserveSeats(boolean forFirstClass, int totalNumberOfSeats) {
    if (forFirstClass == true) {
        bookedFirstClassSeats = totalNumberOfSeats + bookedFirstClassSeats;
        if (bookedFirstClassSeats <= totalFirstClassSeats) {
            return true;
        }
    } else if (forFirstClass == false) {
        bookedCoachSeats = totalNumberOfSeats + bookedCoachSeats;
        if (bookedCoachSeats <= totalCoachSeats) {
            return true;
        }
    } return false;
}
 */


/*
James
    public boolean reserveSeats ( boolean forFirstClass, int totalNumberOfSeats){
        int bookedFirstClass = totalNumberOfSeats + bookedFirstClassSeats;
        if (forFirstClass == true) {
            return true;
        } else if (forFirstClass == false) {
           int bookedCoach = totalNumberOfSeats + bookedCoachSeats;
            return false;
        }
        return false;
    }
}
 */

/*
Joshua
public boolean reserveSeats(boolean forFirstClass, int totalNumberOfSeats) {
        if (forFirstClass) {
            this.bookedFirstClassSeats += totalNumberOfSeats;
        } else {
            this.bookedCoachSeats += totalNumberOfSeats;
        }
        if (forFirstClass && bookedFirstClassSeats <= availableFirstClassSeats) {
            this.availableFirstClassSeats -= totalNumberOfSeats;
            return true;
        } else if (!forFirstClass && bookedCoachSeats <= availableCoachSeats) {
            this.availableCoachSeats -= totalNumberOfSeats;
            return true;
        } else {
            return false;
        }
    }

    //if (forFirstClass && totalNumberofSeats <= AvailableFirstClassSeats)
 */

/*
Loreal
public class Airplane {
    private String planeNumber;
    private int totalFirstClassSeats;
    private int bookedFirstClassSeats;
    private int totalCoachSeats;
    private int bookedCoachSeats;

    public Airplane(String planeNumber, int totalFirstClassSeats, int totalCoachSeats) {
        this.planeNumber = planeNumber;
        this.totalFirstClassSeats = totalFirstClassSeats;
        this.totalCoachSeats = totalCoachSeats;
        }
        public String getPlaneNumber () {
            return planeNumber;
        }
        public int getTotalFirstClassSeats () {
            return totalFirstClassSeats;
        }
    }
}
 */