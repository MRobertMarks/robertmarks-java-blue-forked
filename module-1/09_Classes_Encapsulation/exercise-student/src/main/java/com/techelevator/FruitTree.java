package com.techelevator;

public class FruitTree {

    private String typeOfFruit;
    private int piecesOfFruitLeft;

    public FruitTree(String typeOfFruit, int startingPiecesOfFruit) {
        this.piecesOfFruitLeft = startingPiecesOfFruit;
        this.typeOfFruit = typeOfFruit;
    }

    public String getTypeOfFruit() {
        return typeOfFruit;
    }

    public int getPiecesOfFruitLeft() {
        return piecesOfFruitLeft;
    }

    public boolean pickFruit(int numberOfPiecesToRemove) {
        if (piecesOfFruitLeft - numberOfPiecesToRemove >= 0) {
            piecesOfFruitLeft -= numberOfPiecesToRemove;
            return true;
        } else {
            return false;
        }
    }

}
/*
    private String typeOfFruit;
    private int piecesOfFruitLeft;
    private int startingPiecesOfFruit;
    private int numberOfPiecesToRemove;
    private boolean pickFruit;


    public FruitTree(String typeOfFruit, int startingPiecesOfFruit) {
        this.typeOfFruit = typeOfFruit;
        this.startingPiecesOfFruit = startingPiecesOfFruit;
        this.piecesOfFruitLeft = piecesOfFruitLeft;
        this.numberOfPiecesToRemove = numberOfPiecesToRemove;
        this.pickFruit = pickFruit;

    }
//???
    public String getTypeOfFruit() {
        return typeOfFruit;
//???
    }

    public void setTypeOfFruit(String typeOfFruit) {
        this.typeOfFruit = typeOfFruit;
    }

    public int getPiecesOfFruitLeft() {
        return piecesOfFruitLeft;
    }

    public void setPiecesOfFruitLeft(int piecesOfFruitLeft) {
        this.piecesOfFruitLeft = piecesOfFruitLeft;
    }

    public int getStartingPiecesOfFruit() {
        return startingPiecesOfFruit;
    }

    public void setStartingPiecesOfFruit(int startingPiecesOfFruit) {
        this.startingPiecesOfFruit = startingPiecesOfFruit;
    }

    public int getNumberOfPiecesToRemove() {
        return numberOfPiecesToRemove;
    }

    public void setNumberOfPiecesToRemove(int numberOfPiecesToRemove) {
        this.numberOfPiecesToRemove = numberOfPiecesToRemove;
    }

    public boolean isPickFruit() {
        return pickFruit;
    }

    public void setPickFruit(boolean pickFruit) {
        this.pickFruit = pickFruit;
    }
}

 */
