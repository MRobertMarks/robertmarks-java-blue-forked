package com.techelevator;

public class HomeworkAssignment {


    private int earnedMarks;
    private int possibleMarks;
    private String submitterName;

    public HomeworkAssignment(int possibleMarks, String submitterName) {
        this.possibleMarks = possibleMarks;
        this.submitterName = submitterName;
    }

    public int getPossibleMarks() {
        return possibleMarks;
    }

    public int getEarnedMarks() {
        return earnedMarks;
    }

    public void setEarnedMarks(int earnedMarks) {
        this.earnedMarks = earnedMarks;
    }

    public String getSubmitterName() {
        return submitterName;
    }

    public String getLetterGrade() {
        double percentage = (double) earnedMarks / possibleMarks;
        if (percentage >= 0.9) {
            return "A";
        } else if (percentage >= 0.8) {
            return "B";
        } else if (percentage >= 0.7) {
            return "C";
        } else if (percentage >= 0.6) {
            return "D";
        } else {
            return "F";
        }
    }

}
/*

    //instance variables
    private int earnedMarks;
    private int possibleMarks;
    private String submitterName;
    private String letterGrade; //(Derived)

    public HomeworkAssignment(int possibleMarks, String submitterName) {
        this.earnedMarks = earnedMarks;
        this.possibleMarks = possibleMarks;
        this.submitterName = submitterName;
        this.letterGrade = letterGrade;
    }

    public int getEarnedMarks() {
        return earnedMarks;
    }

    public void setEarnedMarks(int earnedMarks) {
        this.earnedMarks = earnedMarks;
    }

    public int getPossibleMarks() {
        return possibleMarks;
    }

    //public void setPossibleMarks(int possibleMarks) {
        //this.possibleMarks = possibleMarks;
    //}

    public String getSubmitterName() {
        return submitterName;
    }

    //public void setSubmitterName(String submitterName) {
        //this.submitterName = submitterName;
    //}

    public String getLetterGrade() {

        //return ((possibleMarks / earnedMarks) >= 0.90) ? "A" : ((possibleMarks / earnedMarks) >= 0.80) ? "B" : (possibleMarks / earnedMarks >= 0.70) ? "C" : (possibleMarks / earnedMarks >= 0.60) ? "D" : "f";


            if ((possibleMarks / earnedMarks) >= 0.90) {
                return "A";
                // letterGrade = A if possibleMarks / earnedMarks >= 0.90
            } else if ((possibleMarks / earnedMarks) >= 0.80) {
                return "B";
                // letterGrade = B if possibleMarks / earnedMarks >= 0.80
            } else if ((possibleMarks / earnedMarks) >= 0.70) {
                return "C";
                // letterGrade = C if possibleMarks / earnedMarks >= 0.70
            } else if ((possibleMarks / earnedMarks) >= 0.60) {
                return "D";
                // letterGrade = D if possibleMarks / earnedMarks >= 0.60
            }
            return "F";
            // letterGrade = F if possibleMarks / earnedMarks <= 0.59
    }

    public void setLetterGrade(String letterGrade) {
                this.letterGrade = letterGrade;
    }
}


 */